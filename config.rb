########################################################
#
# Middleman - Starting point
#
########################################################



########################################################
#
# Pages, Layouts & Proxies
#
########################################################
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false



########################################################
#
# Asset Directories
#
########################################################
set :css_dir, 'assets/css'
set :js_dir, 'assets/js'
set :images_dir, 'assets/images'
# set :http_prefix, "/Content/images/"


########################################################
#
# Config Variables
#
########################################################
config[:legal_name] = "Starting Point, LLC"
config[:site_name] = "Starting Point"
config[:site_url] = "https://startingpoint.com"
config[:page_transitions] = "no-transition"
config[:stretched] = "stretched"
config[:dark] = ""
config[:footer_logo] = "/assets/images/footer-widget-logo.png"
config[:phone] = "(555)555-5555"
config[:email] = "info@example.org"
config[:address] = "123 4th st"
config[:city] = "Northpole"
config[:state] = "AR"
config[:zip] = "72023"

################# Social links #################
config[:facebook] = "/fb"
config[:twitter] = "/tw"
config[:google_plus] = "/gp"
config[:pintrest] = "/p"
config[:vimeo] = "/v"
config[:linkedin] = "/li"

#Twitter 
config[:twitter_site] = "@yoursite"

# at least 280 x 150 pixels and no more than 1MB
config[:twitter_fb_large_image] = "https://startingpoint.com/images/twitter_large_image.jpg"

#OG Tags
config[:og_app_id] = "952268334783316"


########################################################
#
# Helpers
#
########################################################
# activate canvas gem
activate :canvas

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end


########################################################
#
# Google Analytics
#
########################################################

activate :google_analytics do |ga|
  # Property ID (default = nil)
  ga.tracking_id = 'UA-XXXXXXX-X'

  # Removing the last octet of the IP address (default = false)
  ga.anonymize_ip = false

  # Tracking across a domain and its subdomains (default = nil)
  # ga.domain_name = 'example.com'

  # Tracking across multiple domains and subdomains (default = false)
  ga.allow_linker = false

  # Enhanced Link Attribution (default = false)
  ga.enhanced_link_attribution = false

  # Tracking Code Debugger (default = false)
  ga.debug = false

  # Tracking in development environment (default = true)
  ga.development = false

  # Compress the JavaScript code (default = false)
  ga.minify = true

  # Output style - :html includes <script> tag (default = :html)
  ga.output = :html
end



########################################################
#
# CDN
#
########################################################
# activate :cdn do |cdn|
#   # cdn.cloudflare = {
#   #   client_api_key: '...',          # default ENV['CLOUDFLARE_CLIENT_API_KEY']
#   #   email: 'you@example.com',       # default ENV['CLOUDFLARE_EMAIL']
#   #   zone: 'example.com',
#   #   base_urls: [
#   #     'http://example.com',
#   #     'https://example.com',
#   #   ]
#   # }
#   # cdn.maxcdn = {
#   #   alias: "...",                   # default ENV['MAXCDN_ALIAS']
#   #   consumer_key: "...",            # default ENV['MAXCDN_CONSUMER_KEY']
#   #   consumer_secret: "...",         # default ENV['MAXCDN_CONSUMER_SECRET']
#   #   zone_id: "...",
#   # }
#   # cdn.fastly = {
#   #   api_key: '...',                 # default ENV['FASTLY_API_KEY']
#   #   base_urls: [
#   #     'http://www.example.com',
#   #     'https://www.example.com'
#   #   ],
#   # }
#   cdn.cloudfront = {
#     access_key_id: '',           # default ENV['AWS_ACCESS_KEY_ID']
#     secret_access_key: '',       # default ENV['AWS_SECRET_ACCESS_KEY']
#     distribution_id: ''
#   }
#   # cdn.rackspace = {
#   #   username: "...",                # default ENV['RACKSPACE_USERNAME']
#   #   api_key: "...",                 # default ENV['RACKSPACE_API_KEY']
#   #   region: "DFW",                  # DFW, SYD, IAD, ORD, HKG, etc
#   #   container: "...",
#   #   notification_email: "you@example.com" # optional
#   # }
#   cdn.filter            = /\.html.gz/i # default /.*/
#   cdn.after_build       = true      # default is false
# end


########################################################
#
# Development Configs
#
########################################################

configure :development do

  # refresh on page update
  activate :livereload

  # remove .html extension
  activate :directory_indexes

# End development  
end




########################################################
#
# Build Configs
#
########################################################
configure :build do

  # Minify CSS on build
  activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript

  # Enable cache buster... unique css name, etc
  activate :asset_hash

  # Use relative URLs
  activate :relative_assets

  # remove .html extension
  activate :directory_indexes

  # Compress/gzip
 	activate :gzip



  ################# Stuff below this line isn't compatible with v4 #################
  
  # Compress Images
  # activate :imageoptim 
  
  ################# Start HTML Min #################
  activate :minify_html do |html|
    html.remove_multi_spaces        = true   # Remove multiple spaces
    html.remove_comments            = true   # Remove comments
    html.remove_intertag_spaces     = false  # Remove inter-tag spaces
    html.remove_quotes              = true   # Remove quotes
    html.simple_doctype             = false  # Use simple doctype
    html.remove_script_attributes   = true   # Remove script attributes
    html.remove_style_attributes    = true   # Remove style attributes
    html.remove_link_attributes     = true   # Remove link attributes
    html.remove_form_attributes     = false  # Remove form attributes
    html.remove_input_attributes    = true   # Remove input attributes
    html.remove_javascript_protocol = true   # Remove JS protocol
    html.remove_http_protocol       = false  # Remove HTTP protocol
    html.remove_https_protocol      = false  # Remove HTTPS protocol
    html.preserve_line_breaks       = false  # Preserve line breaks
    html.simple_boolean_attributes  = true   # Use simple boolean attributes
    html.preserve_patterns          = nil    # Patterns to preserve
  end
 
end


########################################################
#
# S3 Sync
#
########################################################
activate :s3_sync do |s3_sync|
  s3_sync.bucket                     = '' # The name of the S3 bucket you are targeting. This is globally unique.
  s3_sync.region                     = ''     # The AWS region for your bucket.
  s3_sync.aws_access_key_id          = ''
  s3_sync.aws_secret_access_key      = ''
  s3_sync.delete                     = false # We delete stray files by default.
  s3_sync.after_build                = false # We do not chain after the build step by default.
  s3_sync.prefer_gzip                = true
  s3_sync.path_style                 = true
  s3_sync.reduced_redundancy_storage = false
  s3_sync.acl                        = 'public-read'
  s3_sync.encryption                 = false
  s3_sync.prefix                     = ''
  s3_sync.version_bucket             = true
  s3_sync.index_document             = 'index.html'
  s3_sync.error_document             = '404.html'
end